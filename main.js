var homeClick = function (event) { $( "#body" ).load("ajax/home.html"); };
var eduClick = function (event) { $( "#body" ).load("ajax/edu.html"); };
var projClick = function (event) { $( "#body" ).load("ajax/proj.html"); };
var skilClick = function (event) { $( "#body" ).load("ajax/skil.html"); };
var contClick =  function (event) { $( "#body" ).load("ajax/cont.html"); };

document.getElementById("home").addEventListener('click', homeClick);
document.getElementById("education").addEventListener('click', eduClick);
document.getElementById("projects").addEventListener('click', projClick);
document.getElementById("skills").addEventListener('click', skilClick);
document.getElementById("contact").addEventListener('click', contClick);

homeClick();
